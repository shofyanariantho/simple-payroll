<div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-3">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link @yield('sb-dashboard')">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/employees" class="nav-link @yield('sb-employees')">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Employees
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/departments" class="nav-link @yield('sb-departments')">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Departments
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/positions" class="nav-link @yield('sb-positions')">
              <i class="nav-icon fas fa-user-plus"></i>
              <p>
                Positions
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="/admin/input" class="nav-link @yield('sb-input')">
              <i class="nav-icon fas fa-clipboard-check"></i>
              <p>
                Test Input
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>