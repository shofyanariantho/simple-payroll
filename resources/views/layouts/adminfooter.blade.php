<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Payroll Management System 1.0.0
    </div>
    <!-- Default to the left -->
    <strong>&copy; 2022 <a href="https://shofyan.my.id/">Shofyan Ariantho</a>.</strong> All rights reserved.
</footer>