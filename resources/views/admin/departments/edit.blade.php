@extends('layouts.admin')

@section('welcome', 'Hello, User!')
@section('menu', 'Departments')
@section('title', 'List Data Departemen')
@section('sb-departments', 'active')

@section('content')
<section class="content">
    <div class="container-fluid">
    <div class="row">

        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Edit Departemen</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="/admin/departments/{{$departments->id}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label>Nama Departemen</label>
                            <input type="text" name="name" value="{{$departments->name}}" class="form-control">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="card-action mt-3">
                            <button type="submit" class="btn btn-success">Update</button>
                            <a href="/admin/departments" class="btn btn-warning">Cancel</a>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        
    </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

