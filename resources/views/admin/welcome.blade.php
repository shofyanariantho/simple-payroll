@extends('layouts.admin')

@section('welcome', 'Payroll Management System - Lite')
@section('menu', 'Dashboard')
@section('title', 'Welcome')
@section('sb-dashboard', 'active')

@section('content')
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-12">

        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Welcome, User!</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                Ini adalah Sistem Manajemen Gaji sederhana. Berikut ini adalah fitur-fitur pada yang ada pada Sistem Manajemen Gaji ini: 
                <ul class="mt-2">
                    <li>Rekap data pegawai</li>
                    <li>List Departemen</li>
                    <li>List Posisi Kerja tiap Departemen</li>
                    <li>List Data Gaji & Bonus dari tiap Posisi Kerja</li>
                    <li>Rekap Jumlah Gaji tiap Pegawai</li>
                </ul>
                
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@endsection