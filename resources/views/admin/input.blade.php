@extends('layouts.admin')

@push('style')
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@push('script')
    <script>
    var form = document.getElementById("formConditional")
    var jawaban = document.getElementById("jawaban")
    
    form.addEventListener("submit", function(e) {
        e.preventDefault()

        var input1 = document.getElementById("input1").value
        var input2 = document.getElementById("input2").value

        function match(input1, input2) {
            let count = 0;

            for (let i in input1) {
                input2.toLowerCase().includes(input1[i].toLowerCase()) ? count++ : false;
            }

            return ((count * 100) / input1.length).toFixed(0);
        }

        var jawabanKonsol = "Hasil perhitungan = " + match(input1, input2) + "%"; 
        console.log(match(input1, input2));

        jawaban.innerHTML = jawabanKonsol
    
    })
    </script>
@endpush

@section('welcome', 'Hello, User!')
@section('menu', 'Test Input')
@section('title', 'Free Input')
@section('sb-input', 'active')

@section('content')
<section class="content">
    <div class="container-fluid">
    <div class="row">

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Free Input</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="#" id="formConditional">
                        <div class="form-group">
                            <label>Input 1</label>
                            <input type="text" name="input1" id="input1" class="form-control">
                        </div>
                        
                        <div class="form-group">
                            <label>Input 2</label>
                            <input type="text" name="input2" id="input2" class="form-control">
                        </div>
                    
                        <div class="card-action mt-3">
                            <input type="submit" class="btn btn-success" value="submit"></input>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Result</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div><p>*Sistem akan menghitung berapa persen karakter dari input pertama yang ada di input kedua.</p></div>
                <div class="ml-3 pb-2">
                    <div>
                        <b id="jawaban"></b> 
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

