@extends('layouts.admin')

@push('style')
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@push('script')
    <script src="{{ asset('theme/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

@endpush

@section('welcome', 'Hello, User!')
@section('menu', 'Employees')
@section('title', 'Data Pegawai')
@section('sb-employees', 'active')

@section('content')
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="/admin/employees/{{$employee->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <a href="/admin/employees/" class="btn btn-info btn-sm">Kembali</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nama Pegawai</label>
                                <input type="text" name="fullName" value="{{ $employee->fullName }}" class="form-control" readonly>
                            </div>
                            
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="date" name="birthDate" value="{{ $employee->birthDate }}" class="form-control" readonly>
                            </div>
        
                            <div class="form-group">
                                <label>Gender</label>
                                <input type="text" name="fullName" value="{{ $employee->gender }}" class="form-control" readonly>
                            </div>
        
                            <div class="form-group">
                                <label>Address</label>
                                <textarea name="address" class="form-control" rows="1" readonly>{{ $employee->address }}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Department</label>
                                <input type="text" value="{{ $employee->positions->departments->name }}" class="form-control" readonly>
                            </div>
                            
                            <div class="form-group">
                                <label>Position</label>
                                <input type="text" value="{{ $employee->positions->name }}" class="form-control" readonly>
                            </div>
        
                            <div class="form-group">
                                <label>Salary</label>
                                <input type="text" value="{{ $employee->positions->salary }}" class="form-control" readonly>
                            </div>

                            <div class="form-group">
                                <label>Bonus</label>
                                <input type="text" value="{{ $employee->positions->bonus }}" class="form-control" readonly>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

