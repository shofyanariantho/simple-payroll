@extends('layouts.admin')

@push('style')
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@push('script')
    <script src="{{ asset('theme/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

@endpush

@section('welcome', 'Hello, User!')
@section('menu', 'Employees')
@section('title', 'Data Pegawai')
@section('sb-employees', 'active')

@section('content')
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Tambah Data Pegawai</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="/admin/employees/{{$employee->id}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label>Position</label>
                            <select name="position_id" class="form-control">
                                @foreach ($positions as $item)
                                <option value={{ $item->id }}>{{ $item->departments->name }} - {{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('position_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <label>Nama Pegawai</label>
                            <input type="text" name="fullName" value="{{ $employee->fullName }}" class="form-control">
                        </div>
                        @error('fullName')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" name="birthDate" value="{{ $employee->birthDate }}" class="form-control">
                        </div>
                        @error('birthDate')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <label>Gender</label>
                            <select name="gender" class="form-control">
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                        </div>
                        @error('gender')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror


                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control" rows="3">{{ $employee->address }}</textarea>
                        </div>
                        @error('address')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="card-action mt-3">
                            <button type="submit" class="btn btn-success">Update</button>
                            <a href="/admin/employees" class="btn btn-warning">Cancel</a>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

