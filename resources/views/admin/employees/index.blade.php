@extends('layouts.admin')

@push('style')
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@push('script')
    <script src="{{ asset('theme/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "paging": true,
        "buttons": ["excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
    </script>
    
@endpush

@section('welcome', 'Hello, User!')
@section('menu', 'Employees')
@section('title', 'Data Pegawai')
@section('sb-employees', 'active')

@section('content')
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-12">

        <div class="card">
            <div class="card-header">
            <a href="/admin/employees/create" class="btn btn-success btn-sm">Add New Employee</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Nama Pegawai</th>
                <th>Gender</th>
                <th>Department</th>
                <th>Position</th>
                <th>Amount</th>
                <th>Action</th>
                </tr>
                </thead>
                
                <tbody>
                @forelse ($employee as $item)
                <tr>
                <td>{{$item->fullName}}</td>
                <td>{{$item->gender}}</td>
                <td>{{$item->positions->departments->name}}</td>
                <td>{{$item->positions->name}}</td>
                <td>{{$item->positions->amount}}</td>
                <td>
                    <form action="/admin/employees/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/admin/employees/{{$item->id}}" class="btn btn-info btn-xs">Detail</a>
                        <a href="/admin/employees/{{$item->id}}/edit" class="btn btn-warning btn-xs">Edit</a>
                        <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                    </form>
                </td>
                </tr>
                @empty
                    <h3><strong>Data belum ada!</strong></h3>
                @endforelse
                </tbody>

            </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

