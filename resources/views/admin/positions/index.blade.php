@extends('layouts.admin')

@push('style')
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@push('script')
    <script src="{{ asset('theme/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('theme/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "paging": true,
        "buttons": ["excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    });
    </script>

    <script>
    function sum() {
      var txtFirstNumberValue = document.getElementById('salary').value;
      var txtSecondNumberValue = document.getElementById('bonus').value;
      var result = parseInt(txtFirstNumberValue) + parseInt(txtSecondNumberValue);
      if (!isNaN(result)) {
         document.getElementById('jawaban').value = result;
      }
    }
    </script>
@endpush

@section('welcome', 'Hello, User!')
@section('menu', 'Positions')
@section('title', 'List Data Position')
@section('sb-positions', 'active')

@section('content')
<section class="content">
    <div class="container-fluid">
    <div class="row">

        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Tambah Data Position</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="/admin/positions" method="POST">
                        @csrf

                        <div class="form-group">
                            <label>Departemen</label>
                            <select name="department_id" class="form-control">
                                @foreach ($departments as $item)
                                <option value={{ $item->id }}>{{ $item->name }}</option>
                                @endforeach
                                
                            </select>
                        </div>
                        @error('department_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label>Position</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label>Salary</label>
                            <input type="number" name="salary" id="salary" onkeyup="sum();" class="form-control">
                        </div>
                        @error('salary')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label>Bonus</label>
                            <input type="number" name="bonus"  id="bonus" onkeyup="sum();" class="form-control">
                        </div>
                        @error('bonus')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <label>Amount</label>
                            <input type="number" name="amount" id="jawaban" class="form-control" readonly>
                        </div>
                        @error('amount')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="card-action mt-3">
                            <button type="submit" class="btn btn-success">Add New</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">@yield('title')</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Departemen</th>
                <th>Position</th>
                <th>Salary</th>
                <th>Bonus</th>
                <th>Amount</th>
                <th>Action</th>
                </tr>
                </thead>
                
                <tbody>
                @forelse ($positions as $key => $item)
                <tr>
                <td>{{$item->departments->name}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->salary}}</td>
                <td>{{$item->bonus}}</td>
                <td>{{$item->amount}}</td>
                <td>
                    <form action="/admin/positions/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/admin/positions/{{$item->id}}/edit" class="btn btn-warning btn-xs">Edit</a>
                        <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                    </form>
                </td>
                </tr>
                @empty
                    <h3><strong>Data belum ada!</strong></h3>
                @endforelse
                </tbody>

            </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@endsection

