@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Welcome and enjoy!') }}</div>

                <div class="card-body">
                    @guest
                        @if (Route::has('login'))    
                        {{ __('Log in, please...') }} 
                        @endif
                    @else
                        {{ __('Go to Dashboard, please...') }} <a href="/admin">Here</a>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
