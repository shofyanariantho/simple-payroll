<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InputController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PositionsController;
use App\Http\Controllers\DepartmentsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::group(['prefix'=>'admin', 'middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('admin.welcome');
    });

    Route::resource('employees', EmployeeController::class);

    Route::group(['prefix'=>'departments'], function(){
        Route::get('/', [DepartmentsController::class, 'create']);
        Route::post('/', [DepartmentsController::class, 'store']);
        Route::get('/', [DepartmentsController::class, 'index']); 
        Route::get('/{departments_id}/edit', [DepartmentsController::class, 'edit']);
        Route::put('/{departments_id}', [DepartmentsController::class, 'update']);
        Route::delete('/{departments_id}', [DepartmentsController::class, 'destroy']); 
    });
    
    Route::group(['prefix'=>'positions'], function(){
        Route::get('/', [PositionsController::class, 'create']);
        Route::post('/', [PositionsController::class, 'store']);
        Route::get('/', [PositionsController::class, 'index']); 
        Route::get('/{positions_id}/edit', [PositionsController::class, 'edit']);
        Route::put('/{positions_id}', [PositionsController::class, 'update']);
        Route::delete('/{positions_id}', [PositionsController::class, 'destroy']); 
    });

    Route::get('/input', [InputController::class, 'show']);
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
