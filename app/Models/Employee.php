<?php

namespace App\Models;

use App\Models\Positions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function positions()
    {
        return $this->belongsTo(Positions::class, 'position_id');
    }
}
