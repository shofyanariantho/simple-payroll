<?php

namespace App\Models;

use App\Models\Departments;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Positions extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function departments()
    {
        return $this->belongsTo(Departments::class, 'department_id');
    }

    public function employee()
    {
        return $this->hasOne(employee::class);
    }
    
}
