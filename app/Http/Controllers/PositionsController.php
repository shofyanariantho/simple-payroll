<?php

namespace App\Http\Controllers;

use App\Models\Positions;
use App\Models\Departments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PositionsController extends Controller
{
    public function index()
    {
        $departments = Departments::all();
        $positions = Positions::all();
        return view('admin.positions.index', compact('departments', 'positions'));
    }

    public function create()
    {
        $departments = Departments::all();
        return view('admin.positions.index', compact('departments'));
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required',
            'department_id' => 'required'
        ]);

        $positions = new Positions;
 
        $positions->name = $request->name;
        $positions->salary = $request->salary;
        $positions->bonus = $request->bonus;
        $positions->amount = $request->amount;
        $positions->department_id = $request->department_id;
 
        $positions->save();

        return redirect('/admin/positions');
    }

    public function edit($positions_id) {
        $departments = Departments::all();
        $positions = Positions::where('id', $positions_id)->first();
        return view('admin.positions.edit', compact('departments', 'positions'));
    }

    public function update(Request $request, $positions_id) {
        $validatedData = $request->validate([
            'name' => 'required',
            'department_id' => 'required'
        ]);
        
        $positions = Positions::find($positions_id);
        
        $positions->name = $request['name'];
        $positions->salary = $request['salary'];
        $positions->bonus = $request['bonus'];
        $positions->amount = $request['amount'];
        $positions->department_id = $request['department_id'];

        $positions->save();

        return redirect('/admin/positions');
    }

    public function destroy($positions_id) {
        $positions = Positions::find($positions_id);
 
        $positions->delete();

        return redirect('/admin/positions');
    }

}   
