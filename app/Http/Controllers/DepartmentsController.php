<?php

namespace App\Http\Controllers;

use App\Models\Departments;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    public function create() {
        return view('admin.departments.index');
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $departments = new Departments;
 
        $departments->name = $request->name;
 
        $departments->save();

        return redirect('/admin/departments');
    }

    public function index() {
        $departments = Departments::all();
        return view('admin.departments.index', compact('departments'));
    }

    public function edit($departments_id) {
        $departments = Departments::where('id', $departments_id)->first();
        return view('admin.departments.edit', compact('departments'));
    }

    public function update(Request $request, $departments_id) {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);
        
        $departments = Departments::find($departments_id);
        
        $departments->name = $request['name'];

        $departments->save();

        return redirect('/admin/departments');
    }

    public function destroy($departments_id) {
        $departments = Departments::find($departments_id);
 
        $departments->delete();

        return redirect('/admin/departments');
    }
}
