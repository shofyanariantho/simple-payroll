<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Positions;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Positions::all();
        $employee = Employee::all();
        return view('admin.employees.index', compact('positions', 'employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = Positions::all();
        return view('admin.employees.create', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'fullName' => 'required',
            'birthDate' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'position_id' => 'required'
        ]);

        $employee = new Employee;
 
        $employee->fullName = $request->fullName;
        $employee->birthDate = $request->birthDate;
        $employee->gender = $request->gender;
        $employee->address = $request->address;
        $employee->position_id = $request->position_id;
        
        $employee->save();

        return redirect('/admin/employees');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $positions = Positions::all();
        $employee = Employee::findOrFail($id);
        return view('admin.employees.detail', compact('positions', 'employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($employee_id)
    {
        $positions = Positions::all();
        $employee = Employee::where('id', $employee_id)->first();
        return view('admin.employees.edit', compact('positions', 'employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $employee_id)
    {
        $validatedData = $request->validate([
            'fullName' => 'required',
            'birthDate' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'position_id' => 'required'
        ]);
        
        $employee = Employee::find($employee_id);
        
        $employee->fullName = $request['fullName'];
        $employee->birthDate = $request['birthDate'];
        $employee->gender = $request['gender'];
        $employee->address = $request['address'];
        $employee->position_id = $request['position_id'];

        $employee->save();

        return redirect('/admin/employees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($employee_id)
    {
        $employee = Employee::find($employee_id);
 
        $employee->delete();

        return redirect('/admin/employees');
    }
}
